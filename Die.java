import java.util.Random;

public class Die {
	private int faceValue;
	private Random rng;
	
	public Die(){
		this.faceValue = 1;
		this.rng = new Random();
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue = rng.nextInt(6) + 1;
	}
	
	public String toString() {
		return "" + this.faceValue;
		
	}
}
