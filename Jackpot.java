import java.util.Scanner;

public class Jackpot {
	public static void main(String[] args) {
		System.out.println("Welcome to Jackpot!");
		Board board = new Board();
		Scanner kb = new Scanner(System.in);
		boolean gameOver;
		boolean playAgain = true;
		int numberOfTilesClosed;
		int gamesWon = 0;
		
		while(playAgain) {
			numberOfTilesClosed = 0;
			gameOver = false;
					
			while(!gameOver) {
				System.out.println(board);
				gameOver = board.playATurn();
				
				if(!gameOver)
					numberOfTilesClosed++;
			}
			
			if(numberOfTilesClosed >= 7) {
				System.out.println("You have reached the Jackpot and won!!");
				gamesWon++;
			}else
				System.out.println("You failed to reach the Jackpot. You lost :(");
			
			System.out.println("Would you like to play again? (Y/N)");
			
			if(kb.nextLine().toUpperCase().charAt(0) == 'Y') {
				System.out.println("Starting a new game!");
				board.reset();
			}else {
				System.out.println("You won " + gamesWon + " times during this session");
				System.out.println("Exiting game");
				playAgain = false;
			}
		}
		
	}
}
