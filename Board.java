
public class Board {
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn() {
		this.die1.roll();
		this.die2.roll();
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		System.out.println("dice : " + this.die1 + "\t" + this.die2);
		
		if(!tiles[sumOfDice - 1]) {
			System.out.println("Closing tile equal to sum : " + (sumOfDice));
			tiles[sumOfDice - 1] = true;
			return false;
		}else if(!tiles[die1.getFaceValue() - 1]) {
			System.out.println("Closing tile with the same value as die 1 : " + (die1.getFaceValue()));
			tiles[die1.getFaceValue() - 1] = true;
			return false;
		}else if(!tiles[die2.getFaceValue() - 1]) {
			System.out.println("Closing tile with the same value as die 2 : " + (die2.getFaceValue()));
			tiles[die2.getFaceValue() - 1] = true;
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut");
		return true;
	}
	
	public void reset() {
		for(int i = 0; i < tiles.length; i++)
			this.tiles[i] = false;
	}
	
	public String toString() {
		String toReturn = "";
		for(int i = 0; i < tiles.length; i++) {
			if(tiles[i])
				toReturn += "X ";
			else
				toReturn += (i + 1) + " ";
		}
		
		return toReturn;
	}
}
